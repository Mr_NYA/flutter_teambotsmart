import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'button.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:ws_communication/servicesChatBot.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var typeTheme = true;
  var color1 = Colors.amber;
  var color2 = Colors.blue;
  static const URL_GoogleAssistant =
      'http://149.202.194.24:3003/assistant/fr-FR/';
  static const URL_PondoraBot =
      'https://www.pandorabots.com/pandora/talk-xml?botid=dbf74f507e3458e4&input=';
  static const URL_Teava = 'https://teamnet-rd.ml/voicebot-api';
  static const Token_Teava = 'cLt9yYygYtVY6rCuQt1F';
  static const Token_DialogFlow = '7a7edfee5c4c4550a609d0580fbb08a3';

 

 



  chatBotDialogFlow(String message) {}

  Future<void> appelChatBot() async {
//  Timer(Duration(seconds: 1), ()
//  {
//    setState(() {
//    this.typeTheme = !this.typeTheme;
// print(typeTheme);

//  });
//   this.appelChatBot();
// });

///////////////////////////////////////////////////////

// int i =0;
// Timer.periodic(Duration(seconds: 2), (timer){

//   DateTime date;
// var time = Timer.periodic(Duration(seconds: 1), (timer) {
//   i++;
//   print('incrementation de $i');
//  date = DateTime.now();
//   print(date);
// });

// Timer(Duration(milliseconds: 1000), (){
//   print('i well stop timer');
//   print(DateTime.now().difference(date));
// time.cancel();
// });
// });

////////////////////////////////////////
    ///
    ///
    ///
    ///
    try {
      var body = jsonEncode({
        "format": "json",
        "token": "cLt9yYygYtVY6rCuQt1F",
        "message": "quel est la météo"
      });
      var response =
          await http.post("https://teamnet-rd.ml/voicebot-api", body: body);
      if (response.statusCode == 200) {
        var jsonResponse = convert.jsonDecode(response.body);
        var output = jsonResponse['that'];
        print('reponse du ChatBot: $output.');
      } else {
        var stat = response.statusCode;
        print('erreur d\'appel $stat');
        print(response);
      }
    } finally {
      print('appel terminé');
    }
  }

  Future<http.Response> postRequest(req) async {
    var url = 'https://teamnet-rd.ml/voicebot-api';

    Map data = {
      "format": "json",
      "token": "cLt9yYygYtVY6rCuQt1F",
      "message": req
    };
    //encode Map to JSON
    var body = json.encode(data);

    var response = await http.post(url,
        headers: {"Content-Type": "application/json"}, body: body);
    print("${response.statusCode}");
    print("${response.body}");
    var msg = convert.jsonDecode(response.body)['that'];
    print('la reponse est ==> $msg');
    return response;
  }

  List<Widget> getLayoutElements() {
    return <Widget>[
      Flexible(
        flex: 4,
        child: Container(
          color: Colors.blue,
        ),
      ),
      Flexible(
        flex: 3,
        child: Container(
          color: Colors.red,
        ),
      ),
      Flexible(
        flex: 3,
        child: Container(
          color: Colors.green,
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
//  Orientation orientation = MediaQuery.of(context).orientation;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(widget.title),
          backgroundColor: (typeTheme) ? color1 : color2,
        ),
        body: Center(
        
          child: MyButton('Clic Here', (typeTheme) ? color1 : color2),
        ),
        bottomNavigationBar: BottomAppBar(
          shape: const CircularNotchedRectangle(),
          child: Container(
            height: 50,
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            var resp = await ServicesChatBot().chatBotGoogleAssistant('la meteo a oujda');
            print('la reponse du ChatBot : $resp');
          },
          child: Icon(Icons.mic_off, color: (typeTheme) ? color1 : color2),
          backgroundColor: Colors.white,
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      ),
    );
  }
}
